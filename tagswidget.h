#ifndef TAGSDOCK_H
#define TAGSDOCK_H
#include "tag_tree_model.h"
#include <QDockWidget>
#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui {
class TagsWidget;
}
QT_END_NAMESPACE

class TagsWidget : public QWidget {
  Q_OBJECT
public:
  TagsWidget(QWidget *parent = nullptr);
  ~TagsWidget();

  void setModel(TagTreeModel *);
  void addTag();

private:
  Ui::TagsWidget *ui;
};

#endif // TAGSDOCK_H
