﻿#include "tag_tree_model.h"
#include <algorithm>
#include <functional>
#include <memory>

Q_LOGGING_CATEGORY(tagmodel, "tagmodel")

#define debug_ qCDebug(tagmodel)

using namespace std::placeholders;

TagTreeModel::TagTreeModel(std::shared_ptr<QNetworkAccessManager> net,
                           QObject *parent)
    : QAbstractItemModel(parent), root(new Tag()),
      loader(new QueueLoader(net, 4, tagmodel()))

{
  debug_ << "init";
}

Tag *TagTreeModel::getTag(const QModelIndex &index) const {
  if (index.isValid()) {
    Tag *tag = static_cast<Tag *>(index.internalPointer());
    if (tag) {
      return tag;
    }
  }
  return root;
}

QModelIndex TagTreeModel::index(int row, int column,
                                const QModelIndex &parent) const {
  //  if (parent.isValid() && parent.column() != 0)
  //    return QModelIndex();
  Tag *parentTag = getTag(parent);
  if (!parentTag) {
    return QModelIndex();
  }
  Tag *tag = parentTag->child(row);
  if (tag) {
    return createIndex(row, column, tag);
  }
  return QModelIndex();
}

QModelIndex TagTreeModel::parent(const QModelIndex &index) const {
  if (!index.isValid()) {
    return QModelIndex();
  }
  Tag *tag = getTag(index);
  if (!tag) {
    return QModelIndex();
  }
  Tag *parent = tag->parent();
  if (!parent) {
    return QModelIndex();
  }
  Tag *grandParent = parent->parent();
  if (!grandParent) {
    return QModelIndex();
  }
  return createIndex(grandParent->childNumber(parent), 0, parent);
}

int TagTreeModel::rowCount(const QModelIndex &parent) const {
  //  if (!parent.isValid())
  //    return 0;
  Tag *parentTag = getTag(parent);
  return parentTag ? parentTag->childCount() : 0;

  //  if (!parentTag) {
  //    return 0;
  //  }
  //  return parentTag->childCount();
}

int TagTreeModel::columnCount(const QModelIndex &parent) const {
  Q_UNUSED(parent);
  return 1;
}

bool TagTreeModel::hasChildren(const QModelIndex &parent) const {
  //  if (!parent.isValid()) {
  //    return false;
  //  }
  Tag *parentTag = getTag(parent);
  //  if (!parentTag) {
  //    return false;
  //  }
  //  return parentTag->childCount() > 0;

  return parentTag ? !parentTag->loaded() || (parentTag->childCount() > 0)
                   : false;
}

bool TagTreeModel::canFetchMore(const QModelIndex &parent) const {
  //  debug_ << "canFetchMore";
  //  return parent.isValid();
  Q_UNUSED(parent);
  return true;
}

void TagTreeModel::fetchMore(const QModelIndex &parent) {

  //  if (!parent.isValid()) {
  //    return;
  //  }
  Tag *parentTag = getTag(parent);
  debug_ << "fetchMore:"
         << (!parentTag ? "null" : QString::number(parentTag->id()));
  if (!parentTag) {
    return;
  }

  int startIndex =
      parentTag->children().isEmpty() ? 0 : parentTag->children().last()->id();

  auto cmd = std::make_unique<
      LoadPtr<Tag *, std::function<void(Tag *, QVector<Tag *>)>>>(
      parentTag, startIndex,
      std::bind(&TagTreeModel::appendedItems, this, _1, _2));

  loader->run(std::move(cmd));
}

void TagTreeModel::appendedItems(Tag *tag, QVector<Tag *> newItems) {
  debug_ << "appendedItems:" << newItems.count() << " items";

  tag->setLoaded();
  if (newItems.isEmpty()) {
    return;
  }

  QModelIndex parent =
      !tag->parent() ? QModelIndex()
                     : createIndex(tag->parent()->childNumber(tag), 0, tag);

  beginInsertRows(parent, tag->childCount(),
                  tag->childCount() + newItems.size() - 1);
  tag->insertChildren(std::move(newItems));
  endInsertRows();
}

QVariant TagTreeModel::data(const QModelIndex &index, int role) const {
  if (!index.isValid()) {
    return QVariant();
  }

  Tag *tag = getTag(index);
  if (!tag) {
    return QVariant();
  }
  if (role == Qt::DisplayRole) {
    return tag->name();
  }

  return QVariant();
}

Qt::ItemFlags TagTreeModel::flags(const QModelIndex &index) const {
  if (!index.isValid())
    return Qt::NoItemFlags;

  return QAbstractItemModel::flags(index);
}

// bool TagTreeModel::setData(const QModelIndex &index, const QVariant &value,
// int role)
//{
//    if (data(index, role) != value) {
//        // FIXME: Implement me!
//        emit dataChanged(index, index, QVector<int>() << role);
//        return true;
//    }
//    return false;
//}

void TagTreeModel::requestInsertionTag(const QString &name,
                                       const QModelIndex &parent) {
  Tag *parentTag = getTag(parent);
  auto cmd = std::make_unique<Insert<Tag *, std::function<void(Tag *, Tag *)>>>(
      name, parentTag, std::bind(&TagTreeModel::insertTag, this, _1, _2));

  loader->run(std::move(cmd));
}

void TagTreeModel::insertTag(Tag *newTag, Tag *parentTag) {

  QVector<Tag *>::const_iterator begin = parentTag->children().cbegin();
  auto loBound =
      std::lower_bound(begin, parentTag->children().cend(), newTag,
                       [](Tag *x, Tag *y) { return (x->id() < y->id()); });
  int pos = loBound == begin ? 0 : (loBound - begin);

  auto parent = !parentTag->parent()
                    ? QModelIndex()
                    : createIndex(parentTag->parent()->childNumber(parentTag),
                                  0, parentTag);
  debug_ << "insert pos:" << pos;
  beginInsertRows(parent, pos, pos);
  parentTag->insertChild(newTag, pos);
  endInsertRows();
}

bool TagTreeModel::insertRows(int row, int count, const QModelIndex &parent) {
  beginInsertRows(parent, row, row + count - 1);
  // FIXME: Implement me!
  endInsertRows();
}

// bool TagTreeModel::insertColumns(int column, int count, const QModelIndex
// &parent)
//{
//    beginInsertColumns(parent, column, column + count - 1);
//    // FIXME: Implement me!
//    endInsertColumns();
//}

// bool TagTreeModel::removeRows(int row, int count, const QModelIndex &parent)
//{
//    beginRemoveRows(parent, row, row + count - 1);
//    // FIXME: Implement me!
//    endRemoveRows();
//}

// bool TagTreeModel::removeColumns(int column, int count, const QModelIndex
// &parent)
//{
//    beginRemoveColumns(parent, column, column + count - 1);
//    // FIXME: Implement me!
//    endRemoveColumns();
//}
