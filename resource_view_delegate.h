#ifndef RESOURCEVIEWDELEGATE_H
#define RESOURCEVIEWDELEGATE_H

#include <QStyledItemDelegate>

class ResourceViewDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    ResourceViewDelegate(QObject *parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionViewItem &option,
                       const QModelIndex &index) const override ;
    QSize sizeHint(const QStyleOptionViewItem &option,
                             const QModelIndex &index) const override;

signals:
    void paintItem( const QModelIndex &index ) const ;


};

#endif // RESOURCEVIEWDELEGATE_H
