#ifndef RESOURCEVIEW_H
#define RESOURCEVIEW_H

#include <QListView>

class ResourceView : public QListView
{
    Q_OBJECT
public:
    ResourceView( QWidget *parent = nullptr );

public slots:
   void paintedIndex( const QModelIndex &index ) ;

signals:
    void itemPaint(const QModelIndex &index);
};

#endif // RESOURCEVIEW_H
