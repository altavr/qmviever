
#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(std::shared_ptr<QNetworkAccessManager> net,
                       QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow),
      model(std::make_shared<MListModel>(net)),
      tagmodel(std::make_shared<TagTreeModel>(net)), listDelegate(nullptr) {
  ui->setupUi(this);
  listDelegate = new ResourceViewDelegate();

  ui->resourcesView->setItemDelegate(listDelegate);
  ui->resourcesView->setModel(model.get());
  connect(listDelegate, &ResourceViewDelegate::paintItem, ui->resourcesView,
          &ResourceView::paintedIndex);
  connect(ui->resourcesView, &ResourceView::itemPaint, model.get(),
          &MListModel::fetchThumbs);

  ui->tagswidget->setModel(tagmodel.get());
}

MainWindow::~MainWindow() {
  delete ui;
  delete listDelegate;
}
