#include "mainwindow.h"

//#include "mlist_model.h"
//#include "resource.h"
//#include "tag_tree_model.h"
#include <QApplication>
#include <QNetworkAccessManager>
#include <memory>

void init() {

  Resource::placeholder = new QPixmap(":/icons/images/nopic96.png");
}

int main(int argc, char *argv[]) {

  QApplication app(argc, argv);
  init();

  QLoggingCategory::setFilterRules("*.debug=false\n"
                                   "mlmodel.debug=true\n"
                                   "tagmodel.debug=false\n");

  auto network = std::make_shared<QNetworkAccessManager>();

  MainWindow win(network);
  win.show();
  return app.exec();
}
