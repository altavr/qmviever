#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "mlist_model.h"
#include "resource_view_delegate.h"
#include "tag_tree_model.h"
#include <QMainWindow>
#include <memory>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(std::shared_ptr<QNetworkAccessManager> net,
             QWidget *parent = nullptr);
  ~MainWindow();

private:
  Ui::MainWindow *ui;
  std::shared_ptr<MListModel> model;
  std::shared_ptr<TagTreeModel> tagmodel;
  ResourceViewDelegate *listDelegate;
};
#endif // MAINWINDOW_H
