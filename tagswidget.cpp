#include "tagswidget.h"

#include "ui_tagswidget.h"
#include <QInputDialog>

TagsWidget::TagsWidget(QWidget *parent)
    : QWidget(parent), ui(new Ui::TagsWidget) {
  ui->setupUi(this);
  connect(ui->addButton, &QToolButton::clicked, this, &TagsWidget::addTag);
}

TagsWidget::~TagsWidget() { delete ui; }
void TagsWidget::setModel(TagTreeModel *model) { ui->tagView->setModel(model); }

void TagsWidget::addTag() {

  bool ok;
  QString text = QInputDialog::getText(this, tr("New tag"), tr("Tag name:"),
                                       QLineEdit::Normal, "", &ok);
  if (!ok || text.isEmpty()) {
    return;
  }

  TagTreeModel *model = qobject_cast<TagTreeModel *>(ui->tagView->model());
  if (!model) {
    return;
  }
  model->requestInsertionTag(text, ui->tagView->currentIndex());
}
