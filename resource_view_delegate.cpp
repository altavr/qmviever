#include "resource_view_delegate.h"
#include "resource.h"
#include <iostream>
#include <QPainter>

ResourceViewDelegate::ResourceViewDelegate(QObject *parent ) :
    QStyledItemDelegate(parent)
{

}

void ResourceViewDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                                 const QModelIndex &index) const {



    auto item = index.data();
    if ( item.canConvert<Resource>() ) {
//        std::cout << "paint " << index.row() << "\n"  ;
        //        std::cout << "paint\n" ;
        auto resource = qvariant_cast<Resource>(item);
        if ( !resource.thumbLoaded() ) {
            emit paintItem(index);
        }

        if (option.state & QStyle::State_Selected)
            painter->fillRect(option.rect, option.palette.highlight());
        resource.paint( painter, option.rect, option );

    } else {
//        std::cout << "select " << index.row() << "\n"  ;
        QStyledItemDelegate::paint( painter, option, index );
    }
}

QSize ResourceViewDelegate::sizeHint(const QStyleOptionViewItem &option,
                                     const QModelIndex &index) const  {

    auto item = index.data();
    if ( item.canConvert<Resource>() ) {
        auto resource = qvariant_cast<Resource>(item);
        return  resource.sizeHint( option.fontMetrics );

    }
    return QStyledItemDelegate::sizeHint(  option, index );

}
