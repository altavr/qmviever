#include "resource.h"
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QMap>
#include <QPainter>
#include <QPalette>
#include <QVariant>
#include <iostream>

Thumb::Thumb(int res_id) : resource_id(res_id) {}
QString Thumb::loadPath() {
  return "/resources/" + QString::number(resource_id) + "/thumbnails";
}
QString Thumb::fromJson(const QJsonValue &j, Thumb *) { return j.toString(); }

Resource::Resource(QVector<QVariant> &&data) : m_data(data) {}

QPixmap *Resource::placeholder = nullptr;

int Resource::id() const { return m_data[0].toInt(); }

QSize Resource::drawThumbs(QPainter *painter) const {

  int xc = 0;
  int i = 0;
  int width = placeholder->width();
  int height = placeholder->height();
  //    qDebug() << "drawThumbs " << pics.count();
  for (auto pic : pics) {
    ++i;
    int delta = pic.width() - width;
    if (delta <= 0) {
      painter->fillRect(xc, 0, width, height, Qt::black);
      painter->drawPixmap(xc + (-delta) / 2, 0, pic);
    } else {
      auto frag =
          QPainter::PixmapFragment::create(QPointF(xc + width / 2, height / 2),
                                           QRectF(delta / 2, 0, width, height));
      painter->drawPixmapFragments(&frag, 1, pic);
    }
    xc += width + thumbSpace;
  }

  for (; i < defaultThumbCount; ++i) {
    painter->drawPixmap(xc, 0, *placeholder);
    xc += width + thumbSpace;
  }

  return QSize(xc - thumbSpace, height);
}

void Resource::drawStringColumn(int x, int y, QPainter *painter,
                                std::initializer_list<int> idxs, int maxWidth,
                                const QFontMetrics &fm) const {

  int yc = y;
  for (int i : idxs) {
    painter->drawText(x, yc, m_data[i].toString());
    yc += fm.lineSpacing();
  }
}

int Resource::drawTextBlock(QPainter *painter, int max_width,
                            const QStyleOptionViewItem &style) const {

  painter->setPen(style.palette.color(QPalette::Text));
  auto fm = style.fontMetrics;
  int linesize = 100;
  drawStringColumn(0, 0, painter, {0}, linesize, fm);
  linesize = 200;
  drawStringColumn(linesize, 0, painter, {1}, linesize, fm);
  linesize = 400;
  drawStringColumn(linesize, 0, painter, {2}, linesize, fm);

  return fm.height();
}

void Resource::paint(QPainter *painter, const QRect &rect,
                     const QStyleOptionViewItem &style) const {

  painter->save();
  painter->setRenderHint(QPainter::Antialiasing, true);

  painter->translate(rect.left() + leftMargin, rect.top() + vMargin);
  auto ts = drawThumbs(painter);
  painter->translate(0,
                     ts.height() + style.fontMetrics.height() + thumbTextSpace);
  drawTextBlock(painter, ts.width(), style);

  painter->restore();
}

QSize Resource::sizeHint(const QFontMetrics &fm) const {

  QSize px = placeholder->size();

  return QSize(2 * leftMargin + defaultThumbCount * px.width() +
                   thumbSpace * (defaultThumbCount - 1),
               2 * vMargin + px.height() + thumbTextSpace + fm.lineSpacing());
}

const QVariant &Resource::data(size_t idx) const { return m_data[idx]; }

void Resource::setThumbs(QVector<QPixmap> &&thumbs) { pics = thumbs; }

bool Resource::thumbLoaded() const { return 0 < pics.count(); }

QString Resource::loadPath() { return "/resources"; }

Resource Resource::fromJson(const QJsonValue &j, Resource *parent) {
  Q_UNUSED(parent);
  auto map = j.toObject().toVariantMap();
  return Resource({map.value("id"), map.value("name"), map.value("url")});
}
