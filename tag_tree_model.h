#ifndef TAGTREEMODEL_H
#define TAGTREEMODEL_H

#include "misc.h"
#include "tag.h"
#include <QAbstractItemModel>
#include <QLoggingCategory>
#include <functional>
#include <utility>

Q_DECLARE_LOGGING_CATEGORY(tagmodel)

class TagTreeModel : public QAbstractItemModel {
  Q_OBJECT

  //  friend LoadPtr<Tag, TagTreeModel>;
  //  friend Insert<Tag, std::function<void(Tag *, Tag *)>>;

public:
  TagTreeModel(std::shared_ptr<QNetworkAccessManager>,
               QObject *parent = nullptr);

  ~TagTreeModel() {
    delete root;
    delete loader;
  }

  // Basic functionality:
  QModelIndex index(int row, int column,
                    const QModelIndex &parent = QModelIndex()) const override;
  QModelIndex parent(const QModelIndex &index) const override;

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;

  // Fetch data dynamically:
  bool hasChildren(const QModelIndex &parent = QModelIndex()) const override;

  bool canFetchMore(const QModelIndex &parent) const override;
  void fetchMore(const QModelIndex &parent) override;

  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;

  Qt::ItemFlags flags(const QModelIndex &index) const override;

  //    // Editable:
  //    bool setData(const QModelIndex &index, const QVariant &value,
  //                 int role = Qt::EditRole) override;

  //    // Add data:
  void requestInsertionTag(const QString &name,
                           const QModelIndex &parent = QModelIndex());

  bool insertRows(int row, int count,
                  const QModelIndex &parent = QModelIndex()) override;

  // bool insertColumns(int column, int count,
  //    const QModelIndex &parent = QModelIndex()) override;

  //    // Remove data:
  //    bool removeRows(int row, int count, const QModelIndex &parent =
  //    QModelIndex()) override; bool removeColumns(int column, int count,
  //    const QModelIndex &parent = QModelIndex()) override;

private:
  //    using  reply_t = std::unique_ptr<QNetworkReply, void(*)(
  //    QNetworkReply*)> ;

  Tag *getTag(const QModelIndex &index) const;
  void appendedItems(Tag *tag, QVector<Tag *>);
  void rowUpdate(int);
  void insertTag(Tag *newTag, Tag *parent);

  //    bool replyIsOK( reply_t &);
  //    QJsonDocument getJson(reply_t &  );

  //    QVector<Tag* > additinalItems( reply_t &reply, Tag* parent);

  //    template <typename T>
  //    void makeRequest(const QNetworkRequest &, T && );

  //    void load(const QModelIndex &parent, reply_t &reply);
  //    void loadMore(const QModelIndex &parent)  ;

  Tag *root;
  //  Tag *currentItem;
  Loader *loader;

  //    QVector<Tag*> queryToLoad;
  //    bool addItemsInProgress;
};

#endif // TAGTREEMODEL_H
