#ifndef TAG_H
#define TAG_H
#include <QString>
#include <QVector>
#include <misc.h>

class Tag {
public:
  explicit Tag();
  Tag(int id, Tag *parent, QString name);
  ~Tag();

  int id() const { return m_id; }
  //    int parent_id() const ;
  const QString &name() const { return m_name; }
  const QVector<Tag *> &children() const { return m_children; };

  Tag *child(int number) const;
  int childCount() const;
  void insertChildren(QVector<Tag *> &&children);
  void insertChild(Tag *c, int pos);
  Tag *parent() const;
  void removeChildren(int position, int count);
  int childNumber(const Tag *child) const;
  void setName(const QString &);
  bool loaded() const;
  void setLoaded();

  QString loadPath();
  static Tag *fromJson(const QJsonValue &j, Tag *parent);
  static Tag *fromJson(const QJsonObject &j, Tag *parent);

private:
  int m_id;
  Tag *m_parent;
  QString m_name;
  QVector<Tag *> m_children;
  bool loadedFlag;
};

#endif // TAG_H
