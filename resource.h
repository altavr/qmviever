#ifndef RESOURCEWIDGET_H
#define RESOURCEWIDGET_H

#include <QFontMetrics>
#include <QPixmap>
#include <QSize>
#include <QStyleOptionViewItem>
#include <initializer_list>

class Thumb {
public:
  Thumb(int resource_id);
  QString loadPath();
  static QString fromJson(const QJsonValue &j, Thumb *parent = nullptr);
  int id() const { return resource_id; }

  int resource_id;
};

class Resource {
public:
  Resource() = default;
  Resource(QVector<QVariant> &&);

  int id() const;
  void paint(QPainter *painter, const QRect &,
             const QStyleOptionViewItem &) const;
  QSize sizeHint(const QFontMetrics &) const;
  const QVariant &data(size_t) const;
  void setThumbs(QVector<QPixmap> &&);
  bool thumbLoaded() const;

  static QPixmap *placeholder;

  QString loadPath();
  static Resource fromJson(const QJsonValue &j, Resource *parent = nullptr);

private:
  static constexpr int defaultThumbCount = 4;

  static constexpr int thumbSpace = 5;
  static constexpr int thumbTextSpace = 2;

  static constexpr int leftMargin = 5;
  static constexpr int vMargin = 10;

  QSize drawThumbs(QPainter *painter) const;
  int drawTextBlock(QPainter *painter, int, const QStyleOptionViewItem &) const;
  void drawStringColumn(int, int, QPainter *painter, std::initializer_list<int>,
                        int maxWidth, const QFontMetrics &fm) const;

  QVector<QPixmap> pics;
  const QVector<QVariant> m_data;
};

Q_DECLARE_METATYPE(Resource)

#endif // RESOURCEWIDGET_H
