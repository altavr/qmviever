#ifndef MISC_H
#define MISC_H
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLoggingCategory>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>
#include <deque>
#include <memory>
#include <type_traits>
#include <vector>

using reply_t = QNetworkReply *;

class Command {
public:
  virtual ~Command(){};
  virtual QNetworkReply *request(QNetworkAccessManager *,
                                 const QLoggingCategory &) = 0;
  virtual void processResult(reply_t &, const QLoggingCategory &) = 0;
  virtual int id() = 0;
  virtual int type() = 0;
};

using cmdPtr = std::unique_ptr<Command>;

class Loader : public QObject {

  Q_OBJECT

public:
  Loader(std::shared_ptr<QNetworkAccessManager> network,
         const QLoggingCategory &, QObject *parent = nullptr);
  virtual ~Loader(){};

  bool run(cmdPtr cmd);

signals:

  void errorOccurred(QString);

protected:
  virtual bool probeQueue(Command *cmd) = 0;
  virtual void deQueue(Command *cmd) = 0;

private:
  void fetch(reply_t reply);

  template <typename F> void makeRequest(QNetworkReply *, F);
  bool replyIsOK(reply_t);

  std::shared_ptr<QNetworkAccessManager> network;
  bool inAction;
  Command *activeCommand = nullptr;
  std::deque<cmdPtr> queue;
  const QLoggingCategory &logCat;
};

class QueueLoader : public Loader {

public:
  QueueLoader(std::shared_ptr<QNetworkAccessManager> network, int len,
              const QLoggingCategory &, QObject *parent = nullptr);
  ~QueueLoader(){};

private:
  bool probeQueue(Command *cmd) override;
  void deQueue(Command *cmd) override;

  QVector<QSet<int>> await;
};

class SingleLoader : public Loader {
public:
  SingleLoader(std::shared_ptr<QNetworkAccessManager> network, int len,
               const QLoggingCategory &, QObject *parent = nullptr);
  ~SingleLoader(){};

private:
  bool probeQueue(Command *cmd) override;
  void deQueue(Command *cmd) override;

  QVector<bool> await;
};

template <typename T, typename F> class Insert : public Command {

public:
  Insert(const QString &name, T parent, F callback);
  ~Insert(){};

  QNetworkReply *request(QNetworkAccessManager *,
                         const QLoggingCategory &) override;
  void processResult(reply_t &, const QLoggingCategory &) override;
  int id() override { return m_parent ? m_parent->id() : 0; }
  int type() override { return 1; }

private:
  QString m_name;
  T m_parent;
  F m_callback;
};

template <typename T, typename F>
Insert<T, F>::Insert(const QString &name, T parent, F callback)
    : m_name(name), m_parent(parent), m_callback(callback) {}

template <typename T, typename F>
QNetworkReply *Insert<T, F>::request(QNetworkAccessManager *net,
                                     const QLoggingCategory &log) {

  QUrl url("http://localhost:8080/tags");

  QUrlQuery post{{"parent_id", QString::number(m_parent->id())},
                 {"name", m_name}};
  auto postData = post.toString(QUrl::FullyEncoded).toUtf8();
  QNetworkRequest request(url);
  request.setHeader(QNetworkRequest::ContentTypeHeader,
                    "application/x-www-form-urlencoded");
  request.setHeader(QNetworkRequest::ContentLengthHeader,
                    QString::number(postData.length()));
  return net->post(request, postData);
}

template <typename T, typename F>
void Insert<T, F>::processResult(reply_t &reply, const QLoggingCategory &log) {
  auto json = QJsonDocument::fromJson(reply->readAll());
  if (!json.isObject()) {
    auto data = json.toJson();
    QString j(data);
    qCDebug(log) << "Json parsing error" << data.length() << j;
    return;
  }
  T newTag = std::remove_pointer<T>::type::fromJson(json.object(), m_parent);
  m_callback(newTag, m_parent);
}

template <typename T, typename F> class Load : public Command {
public:
  Load(T item, int startIndex, F callback);
  virtual ~Load() = default;

  QNetworkReply *request(QNetworkAccessManager *,
                         const QLoggingCategory &) override;
  void processResult(reply_t &, const QLoggingCategory &) override;
  int id() override { return m_item ? m_item->id() : 0; }
  int type() override { return 0; }

protected:
  virtual void toCollection(const QJsonArray &arr) = 0;

  F m_callback;
  T m_item;
  int m_startIndex;

private:
};

template <typename T, typename F> class LoadPtr : public Load<T, F> {
public:
  LoadPtr(T item, int startIndex, F callback)
      : Load<T, F>(item, startIndex, callback) {}
  ~LoadPtr() = default;

  using Load<T, F>::m_item;
  using Load<T, F>::m_callback;

private:
  void toCollection(const QJsonArray &arr) override;
};

template <typename T, typename F> class LoadVal : public Load<T, F> {
public:
  LoadVal(T item, int startIndex, F callback)
      : Load<T, F>(item, startIndex, callback) {}
  ~LoadVal() = default;

  using Load<T, F>::m_item;
  using Load<T, F>::m_callback;

private:
  void toCollection(const QJsonArray &arr) override;
};

template <typename T, typename F> class LoadThumb : public Load<T, F> {
public:
  LoadThumb(T item, int startIndex, F callback, int row)
      : Load<T, F>(std::move(item), startIndex, callback), m_row(row) {}
  ~LoadThumb() = default;

  using Load<T, F>::m_item;
  using Load<T, F>::m_callback;

  //  typedef Load<T, F>::std::remove_pointer<T>::type wo_ptr;

private:
  void toCollection(const QJsonArray &arr) override;
  int m_row;
};

template <typename T, typename F>
Load<T, F>::Load(T tag, int startIndex, F callback)
    : m_item(std::move(tag)), m_startIndex(startIndex), m_callback(callback) {}

template <typename T, typename F>
QNetworkReply *Load<T, F>::request(QNetworkAccessManager *net,
                                   const QLoggingCategory &log) {
  QUrl url = QUrl("http://localhost:8080" + m_item->loadPath());
  url.setQuery(QUrlQuery{
      {"startIndex", QString::number(m_startIndex)},
      {"limit", "10000"},
  });
  qCDebug(log) << "GET:" << url.toString();
  return net->get(QNetworkRequest(url));
}

template <typename T, typename F>
void Load<T, F>::processResult(reply_t &reply, const QLoggingCategory &log) {
  qCDebug(log) << "fetch";

  auto content = reply->readAll();

  qCDebug(log) << "CALL additinalItems";
  auto json = QJsonDocument::fromJson(content);
  if (!json.isArray()) {
    qCDebug(log) << "Json parsing error";
  }
  qCDebug(log) << "get new items:" << json.array().count();
  toCollection(json.array());
}

template <typename T, typename F>
void LoadPtr<T, F>::toCollection(const QJsonArray &arr) {

  QVector<T> list;
  for (auto i : arr) {
    T tag = std::remove_pointer<T>::type::fromJson(i, m_item);
    list.push_back(tag);
  }
  m_callback(m_item, std::move(list));
}

template <typename T, typename F>
void LoadVal<T, F>::toCollection(const QJsonArray &arr) {

  QVector<typename std::remove_pointer<T>::type> list;

  for (auto i : arr) {
    list.push_back(std::remove_pointer<T>::type::fromJson(i, m_item));
  }
  m_callback(m_item, std::move(list));
}

template <typename T, typename F>
void LoadThumb<T, F>::toCollection(const QJsonArray &arr) {

  QVector<QString> list;
  for (auto i : arr) {
    list.push_back(T::element_type::fromJson(i, m_item.get()));
  }
  m_callback(std::move(list), m_row);
}

#endif // MISC_H
