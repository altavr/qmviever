#include "misc.h"
#include "tag.h"
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrl>
#include <QUrlQuery>

Loader::Loader(std::shared_ptr<QNetworkAccessManager> net,
               const QLoggingCategory &log, QObject *parent)
    : QObject(parent), network(net), inAction(false), logCat(log) {}

bool Loader::run(cmdPtr cmd) {

  if (!probeQueue(cmd.get())) {
    return false;
  }

  if (inAction) {
    queue.push_back(std::move(cmd));
    return true;
  }

  activeCommand = cmd.release();
  inAction = true;
  auto reply = activeCommand->request(network.get(), logCat);
  QObject::connect(reply, &QNetworkReply::finished, this, [=]() {
    fetch(reply);
    reply->deleteLater();
  });

  return true;
}

void Loader::fetch(reply_t reply) {
  activeCommand->processResult(reply, logCat);

  deQueue(activeCommand);
  delete activeCommand;
  activeCommand = nullptr;

  if (queue.empty()) {
    inAction = false;
    return;
  }
  activeCommand = queue.front().release();
  queue.pop_front();

  auto nextReply = activeCommand->request(network.get(), logCat);

  QObject::connect(nextReply, &QNetworkReply::finished, this, [=]() {
    fetch(nextReply);
    nextReply->deleteLater();
  });
}

bool Loader::replyIsOK(reply_t reply) {
  if (reply->error() != QNetworkReply::NoError) {
    qCDebug(logCat) << "network error";
    emit errorOccurred(reply->errorString());
    return false;
  }
  return true;
}

QueueLoader::QueueLoader(std::shared_ptr<QNetworkAccessManager> network,
                         int len, const QLoggingCategory &logCat,
                         QObject *parent)
    : Loader(network, logCat, parent), await(QVector<QSet<int>>(len)) {}

bool QueueLoader::probeQueue(Command *cmd) {
  if (await[cmd->type()].contains(cmd->id())) {
    return false;
  }
  await[cmd->type()].insert(cmd->id());
  return true;
}
void QueueLoader::deQueue(Command *cmd) {
  await[cmd->type()].remove(cmd->id());
}

SingleLoader::SingleLoader(std::shared_ptr<QNetworkAccessManager> network,
                           int len, const QLoggingCategory &logCat,
                           QObject *parent)
    : Loader(network, logCat, parent), await(QVector<bool>(len)) {

  for (auto &x : await) {
    x = false;
  }
}

bool SingleLoader::probeQueue(Command *cmd) {
  if (await[cmd->type()]) {
    return false;
  }
  await[cmd->type()] = true;
  return true;
}
void SingleLoader::deQueue(Command *cmd) { await[cmd->type()] = false; }
