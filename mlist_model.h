#ifndef MLISTMODEL_H
#define MLISTMODEL_H

#include "misc.h"
#include "resource.h"
#include <QAbstractListModel>
#include <QLoggingCategory>
#include <QMap>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

Q_DECLARE_LOGGING_CATEGORY(mlmodel)

class MListModel : public QAbstractListModel {
  Q_OBJECT

  friend LoadVal<Resource, MListModel>;

public:
  explicit MListModel(std::shared_ptr<QNetworkAccessManager>,
                      QObject *parent = nullptr);

  ~MListModel();
  // Basic functionality:
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;

  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;

  QHash<int, QByteArray> roleNames() const override;

  //    // Editable:
  //    bool setData(const QModelIndex &index, const QVariant &value,
  //                 int role = Qt::EditRole) override;

  //    Qt::ItemFlags flags(const QModelIndex& index) const override;

  //    // Add data:
  //    bool insertRows(int row, int count, const QModelIndex &parent =
  //    QModelIndex()) override;

  //    // Remove data:
  //    bool removeRows(int row, int count, const QModelIndex &parent =
  //    QModelIndex()) override;

public slots:

  void fetchThumbs(const QModelIndex &index);

signals:

  void errorOccurred(QString);

protected:
  bool canFetchMore(const QModelIndex &parent) const override;
  void fetchMore(const QModelIndex &parent) override;

private:
  enum Field {
    Name = Qt::UserRole + 1,
    Url,

  };

  void appendedItems(Resource *parent, QVector<Resource> newItems);
  void appendedThumbs(QVector<QString>, int row);

  Loader *loader;

  QVector<Resource> datalist;
};

#endif // MLISTMODEL_H
