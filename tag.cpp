#include "tag.h"
#include <QJsonObject>

Tag::Tag() : m_id(0), m_parent(nullptr), m_name(QString()) {}

Tag::Tag(int id, Tag *parent, QString name)
    : m_id(id), m_parent(parent), m_name(name), m_children(QVector<Tag *>()),
      loadedFlag(false) {}

Tag::~Tag() { qDeleteAll(m_children); }

Tag *Tag::child(int number) const { return m_children[number]; }

int Tag::childCount() const { return m_children.count(); }

void Tag::insertChildren(QVector<Tag *> &&children_) {

  for (auto &&x : children_) {
    m_children.push_back(std::move(x));
  }
}

void Tag::insertChild(Tag *c, int pos) { m_children.insert(pos, c); }

Tag *Tag::parent() const { return m_parent; }

void Tag::removeChildren(int position, int count) {

  m_children.remove(position, count);
}

int Tag::childNumber(const Tag *child_) const {

  for (int i = 0; i < m_children.count(); ++i) {
    if (m_children[i] == child_) {
      return i;
    }
  }
  return -1;
}

void Tag::setName(const QString &newName) { m_name = newName; }

bool Tag::loaded() const { return loadedFlag; }
void Tag::setLoaded() { loadedFlag = true; }

Tag *Tag::fromJson(const QJsonObject &j, Tag *parent) {
  auto map = j.toVariantMap();
  return new Tag(map.value("id").toInt(), parent, map.value("name").toString());
}

Tag *Tag::fromJson(const QJsonValue &j, Tag *parent) {
  return fromJson(j.toObject(), parent);
}

QString Tag::loadPath() { return "/tags/" + QString::number(m_id); }
