#include "mlist_model.h"
#include <QByteArray>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValueRef>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
//#include <QPair>
#include <QUrl>
#include <QUrlQuery>
#include <memory>

Q_LOGGING_CATEGORY(mlmodel, "mlmodel")

#define debug_ qCDebug(mlmodel)

using namespace std::placeholders;

MListModel::MListModel(std::shared_ptr<QNetworkAccessManager> net,
                       QObject *parent)
    : QAbstractListModel(parent), loader(new SingleLoader(net, 4, mlmodel())) {}

MListModel::~MListModel() { delete loader; }

int MListModel::rowCount(const QModelIndex &parent) const {
  // For list models only the root node (an invalid parent) should return the
  // list's size. For all other (valid) parents, rowCount() should return 0 so
  // that it does not become a tree model.
  if (parent.isValid())
    return 0;

  return datalist.size();
}

QVariant MListModel::data(const QModelIndex &index, int role) const {
  if (!index.isValid() || index.row() < 0 || index.row() >= datalist.size())
    return QVariant();

  auto item = datalist[index.row()];
  if (Qt::DisplayRole == role)
    return QVariant::fromValue(item);
  else
    return QVariant();
}

QHash<int, QByteArray> MListModel::roleNames() const {
  QHash<int, QByteArray> roles;
  roles[Name] = "name";
  roles[Url] = "url";
  return roles;
}

// bool MListModel::setData(const QModelIndex &index, const QVariant &value, int
// role)
//{
//    if (data(index, role) != value) {
//        // FIXME: Implement me!
//        emit dataChanged(index, index, QVector<int>() << role);
//        return true;
//    }
//    return false;
//}

// Qt::ItemFlags MListModel::flags(const QModelIndex &index) const
//{
//    if (!index.isValid())
//        return Qt::NoItemFlags;

//    return Qt::ItemIsEditable; // FIXME: Implement me!
//}

// bool MListModel::insertRows(int row, int count, const QModelIndex &parent)
//{
//    beginInsertRows(parent, row, row + count - 1);
//    // FIXME: Implement me!
//    endInsertRows();
//}

// bool MListModel::removeRows(int row, int count, const QModelIndex &parent)
//{
//    beginRemoveRows(parent, row, row + count - 1);
//    // FIXME: Implement me!
//    endRemoveRows();
//}

bool MListModel::canFetchMore(const QModelIndex &parent) const {
  debug_ << "CALL canFetchMore";
  return !parent.isValid();
}

void MListModel::fetchMore(const QModelIndex &parent) {
  debug_ << "CALL fetchMore";

  if (parent.isValid())
    return;

  int startIndex = datalist.size() > 0 ? datalist.last().data(0).toInt() : 0;
  auto cmd = std::make_unique<
      LoadVal<Resource *, std::function<void(Resource *, QVector<Resource>)>>>(
      nullptr, startIndex, bind(&MListModel::appendedItems, this, _1, _2));

  loader->run(std::move(cmd));

  //  requestMore(parent, 200);
}

void MListModel::appendedItems(Resource *parent, QVector<Resource> newItems) {

  Q_UNUSED(parent);
  if (newItems.size() == 0)
    return;

  beginInsertRows(QModelIndex(), datalist.size(),
                  datalist.size() + newItems.size() - 1);
  for (auto &&x : newItems) {
    datalist.push_back(std::move(x));
  }
  endInsertRows();
}

void MListModel::fetchThumbs(const QModelIndex &index) {
  if (!index.isValid() || datalist[index.row()].thumbLoaded())
    return;
  debug_ << "CALL fetchThumbs index:" << index.row();

  int resource_id = datalist[index.row()].data(0).toInt();
  auto callback = std::bind(&MListModel::appendedThumbs, this, _1, _2);
  auto cmd =
      std::make_unique<LoadThumb<std::unique_ptr<Thumb>, decltype(callback)>>(
          std::make_unique<Thumb>(resource_id), 0, callback, index.row());

  loader->run(std::move(cmd));
}

void MListModel::appendedThumbs(QVector<QString> v, int row) {
  //  delete thumb;
  QVector<QPixmap> pics;
  for (auto i : v) {
    auto bin = QByteArray::fromBase64(i.toUtf8());
    QPixmap pic;
    pic.loadFromData(bin, "JPG");
    pics.push_back(pic);
  }
  if (v.count() == 0) {
    pics.push_back(*Resource::placeholder);
  }
  datalist[row].setThumbs(std::move(pics));
  auto index = createIndex(row, 0);
  dataChanged(index, index);
}
