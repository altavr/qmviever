#ifndef TAGSDOCK_H
#define TAGSDOCK_H
#include "tag_tree_model.h"
#include <QDockWidget>

class TagsDock : public QDockWidget {
  Q_OBJECT
public:
  TagsDock(QWidget *parent = nullptr);
  ~TagsDock();
  void setModel(TagTreeModel *);
};

#endif // TAGSDOCK_H
